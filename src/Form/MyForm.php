<?php

namespace Drupal\custom_site_settings\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * MyForm controller.
 */
class MyForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'custom_site_settings_my_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */

  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('Please enter your new site name.'),
    ];

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site name:'),
      '#default_value' => \Drupal::config('system.site')->get('name'), //sets default value to current site name.
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;

  }

  /**
   * Validates the title of the form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $title = $form_state->getValue('title');

   if (strval($title) == \Drupal::config('system.site')->get('name')) {
      $form_state->setErrorByName('title', $this->t('The new site name must be different from the current one. Please try again.')); //Generates an error message if the new site name is identical to the current one.
    }

    \Drupal::configFactory()->getEditable('system.site')->set('name', strval($title))->save(); //sets the string value of $title to the new site name in system.site configurations.
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $messenger = \Drupal::messenger();
    $messenger->addMessage('You changed the site name to: ' . \Drupal::config('system.site')->get('name'));

  }

}
