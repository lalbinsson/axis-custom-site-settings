## AXIS CASE: Custom Site Settings in Drupal 8

AXIS case by Lucy Albinsson.

### Process Overview:

- Firstly, I installed Drupal 8 and created a local website and database using Apache, MySQL and PHP in Ubuntu. 

- I created the custom module "Custom Site Settings" in under var/www/html/module/custom by creating the folder "custom_site_settings" containing the files [`custom_site_settings.routing.yml`](custom_site_settings.routing.yml) and [`custom_site_settings.info.yml`](custom_site_settings.info.yml), and made the module accessible at "/custom-site-settings" by setting the path in the routing file.

- I created the form [`MyForm.php`](MyForm.php) in the same directory under "src"/"Form" which is also set in the modules routing file. Here I created the actual form, which extends FormBase and implements FormInterface and therefore the methods getFormId, buildForm and submitForm. 

- The buildForm method creates a text field with the current site name as a default value, using 
```php
'#default_value' => \Drupal::config('system.site')->get('name'),
```
 When the user types an input into text field and hits "Save", the method submitForm is run. 

- The submitForm method sets the site name to the inpus value from the text field using 
```php
\Drupal::configFactory()->getEditable('system.site')->set('name', strval($title))->save();
``` 
The method also displays a message confirming the change. This results in an update of the page with a new default value in the text field and the site name on the entire website being changed and returning to the buildForm method.

---

- Extra: I also implemented the method validateForm, which is run after buildForm and before submitForm, to make sure that all inputs/actions are valid. This is not always necessary for such a simple form, but I wanted to have something to validate out of curiosity and good measures. I chose restrict the users input, making sure it is not identical to the current site name. If the input is the same as the current site name, an Error message is generated, returning to the buildForm method without any changes.
